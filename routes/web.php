<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(["middleware" => ["auth"]], function (){
    Route::get('/', ["as" => "home", "uses" => "HomeController@dashboard"]);

    Route::group(["prefix" => "service", "namespace" => "Service"], function (){
        Route::get('/', ["as" => "Service.index", "uses" => "ServiceController@index"]);
        Route::post('/', ["as" => "Service.store", "uses" => "ServiceController@store"]);
        Route::get('{services_id}', ["as" => "Service.show", "uses" => "ServiceController@show"]);
        Route::get('{services_id}/edit', ["as" => "Service.edit", "uses" => "ServiceController@edit"]);
        Route::put('{services_id}/edit', ["as" => "Service.update", "uses" => "ServiceController@update"]);
        Route::delete('{services_id}', ["as" => "Service.delete", "uses" => "ServiceController@delete"]);

        Route::group(["prefix" => "{services_id}/ligne"], function (){
            Route::post('/', ["as" => "Service.Ligne.store", "uses" => "ServiceLigneController@store"]);
            Route::delete('/', ["as" => "Service.Ligne.delete", "uses" => "ServiceLigneController@delete"]);

        });
        Route::get('ligne/{lignes_id}/event', 'ServiceLigneController@event');
        Route::get('ligne/{lignes_id}/progress', 'ServiceLigneController@progress');
        Route::get('ligne/{lignes_id}/sound/starter', 'ServiceSoundController@starter');
        Route::get('ligne/{lignes_id}/sound/end', 'ServiceSoundController@end');
        Route::get('ligne/{lignes_id}/sound/nextStop', 'ServiceSoundController@nextStop');
        Route::get('ligne/{lignes_id}/sound/stop', 'ServiceSoundController@stop');
    });

    Route::group(["prefix" => "gare", "namespace" => "Gare"], function (){
        Route::get('/', ["as" => "Gare.index", "uses" => "GareController@index"]);
        Route::post('/', ["as" => "Gare.store", "uses" => "GareController@store"]);
        Route::get('{gares_id}', ["as" => "Gare.show", "uses" => "GareController@show"]);
        Route::get('{gares_id}/edit', ["as" => "Gare.edit", "uses" => "GareController@edit"]);
        Route::put('{gares_id}/edit', ["as" => "Gare.update", "uses" => "GareController@update"]);
        Route::delete('{gares_id}', ["as" => "Gare.delete", "uses" => "GareController@delete"]);

        Route::group(["prefix" => "{gares_id}/access"], function (){
            Route::post('/', ["as" => "Gare.Access.store", "uses" => "GareAccessController@store"]);
            Route::delete('{accesses_id}', ["as" => "Gare.Access.delete", "uses" => "GareAccessController@delete"]);

        });
        Route::get('access/listeForTerminus/{startStations_id}', 'GareAccessOtherController@listeForTerminus');
    });

    Route::group(["prefix" => "train", "namespace" => "Train"], function (){
        Route::get('/', ["as" => "Train.index", "uses" => "TrainController@index"]);
        Route::post('/', ["as" => "Train.store", "uses" => "TrainController@store"]);
        Route::delete("{trains_id}", ["as" => "Train.delete", "uses" => "TrainController@delete"]);

        Route::get('listeByType/{types_id}', 'TrainOtherController@listeByType');

        Route::group(["prefix" => "type"], function (){
            Route::get('/', ["as" => "Train.Type.index", "uses" => "TrainTypeController@index"]);
            Route::post('/', ["as" => "Train.Type.store", "uses" => "TrainTypeController@store"]);
        });
    });
});

Auth::routes();

Route::get('logout', ["as" => "logout", "uses" => "Auth\LoginController@logout"]);
Route::get('test', 'testController@test');
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
