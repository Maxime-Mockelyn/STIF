<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
    <title>
        @section('title')
            | {{ env('APP_NAME') }}
        @show
    </title>
    <!-- Bootstrap Core CSS -->
    <link href="/assets/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="/assets/plugins/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">

    @yield("header_styles")

    <!-- animation CSS -->
    <link href="/assets/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="/assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/custom/css/custom.css">
    <!-- color CSS -->
    <link href="/assets/css/colors/megna-dark.css" id="theme" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/custom/icons/interaction/interaction.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="fix-header">
<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
</div>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <nav class="navbar navbar-default navbar-static-top m-b-0">
        <div class="navbar-header">
            <div class="top-left-part">
                <!-- Logo -->
                <a class="logo" href="{{ route('home') }}">
                    <!-- Logo icon image, you can use font-icon also --><b>
                        <!--This is dark logo icon--><img src="/assets/images/admin-logo.png" alt="home" class="dark-logo" /><!--This is light logo icon--><img src="/assets/images/admin-logo-dark.png" alt="home" class="light-logo" />
                    </b>
                    <!-- Logo text image you can use text also --><span class="hidden-xs">
                        <!--This is dark logo text--><!--This is light logo text--><img src="/assets/images/admin-text-dark.png" alt="home" class="light-logo" />
                     </span> </a>
            </div>
            <!-- /Logo -->
            <!-- Search input and Toggle icon -->
            <ul class="nav navbar-top-links navbar-left">
                <li><a href="javascript:void(0)" class="open-close waves-effect waves-light visible-xs"><i class="ti-close ti-menu"></i></a></li>
            </ul>
            <ul class="nav navbar-top-links navbar-right pull-right">
                <li class="dropdown">
                    <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="javascript:void(0)">
                        @if($config->user->avatar == 1)
                            <img src="/assets/custom/img/avatars/{{ $config->user->id }}.jpg" alt="user-img" width="36" class="img-circle">
                        @else
                            <img src="/assets/images/users/user.png" alt="user-img" width="36" class="img-circle">
                        @endif
                        <b class="hidden-xs">{{ $config->user->prenom }}</b>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-user animated flipInY">
                        <li>
                            <div class="dw-user-box">
                                <div class="u-img">
                                    @if($config->user->avatar == 1)
                                        <img src="/assets/custom/img/avatars/{{ $config->user->id }}.jpg">
                                    @else
                                        <img src="/assets/images/users/user.png">
                                    @endif
                                </div>
                                <div class="u-text">
                                    <h4>{{ $config->user->name }}</h4>
                                    <p class="text-muted">{{ $config->user->email }}</p><a href="#" class="btn btn-rounded btn-danger btn-sm">Voir mon profil</a></div>
                            </div>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#"><i class="ti-user"></i> Mon Profil</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{ route('logout') }}"><i class="fa fa-power-off"></i> Déconnexion</a></li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
        </div>
        <!-- /.navbar-header -->
        <!-- /.navbar-top-links -->
        <!-- /.navbar-static-side -->
    </nav>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav">
            <div class="sidebar-head">
                <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span> <span class="hide-menu">Navigation</span></h3> </div>
            <ul class="nav" id="side-menu">
                <li class="user-pro">
                    <a href="javascript:void(0)" class="waves-effect"> <span class="hide-menu"> {{ $config->user->nom }} {{ $config->user->prenom }}<span class="fa arrow"></span></span>
                    </a>
                    <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
                        <li><a href="javascript:void(0)"><i class="ti-user"></i> <span class="hide-menu">My Profile</span></a></li>
                        <li><a href="javascript:void(0)"><i class="ti-wallet"></i> <span class="hide-menu">My Balance</span></a></li>
                        <li><a href="javascript:void(0)"><i class="ti-email"></i> <span class="hide-menu">Inbox</span></a></li>
                        <li><a href="javascript:void(0)"><i class="ti-settings"></i> <span class="hide-menu">Account Setting</span></a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-power-off"></i> <span class="hide-menu">Logout</span></a></li>
                    </ul>
                </li>
                    <li>
                        <a href="{{ route('home') }}" class="waves-effect {!! (Route::is('home') ? 'active':"") !!}">
                            <i  class="mdi mdi-view-dashboard fa-fw"></i>
                            <span class="hide-menu">Tableau de Bord</span>
                        </a>
                    </li>
                <li>
                    <a href="{{ route("Service.index") }}" class="waves-effect {!! (Route::is('Service.index') ? 'active': "") !!}">
                        <i class="mdi mdi-file fa-fw"></i>
                        <span class="hide-menu">
                            Services
                            <span class="fa arrow"></span>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route("Gare.index") }}" class="waves-effect {!! (Route::is('Gare.index') ? 'active': "") !!}">
                        <i class="mdi mdi-bank fa-fw"></i>
                        <span class="hide-menu">
                            Gares
                            <span class="fa arrow"></span>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('Train.index') }}" class="waves-effect {!! (Route::is('Train.index') ? 'active':"") !!}">
                        <i class="mdi mdi-train fa-fw" data-icon="v"></i>
                        <span class="hide-menu"> Trains
                            <span class="fa arrow"></span>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Left Sidebar -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            @include('includes.pageHeader')
            @include('error')
            @yield("content")
        </div>
        <!-- /.container-fluid -->
        <footer class="footer text-center"> 2017 &copy; SRICE </footer>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- /#wrapper -->
<!-- jQuery -->
<script src="/assets/plugins/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="/assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="/assets/plugins/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="/assets/js/jquery.slimscroll.js"></script>
@yield("footer_scripts")
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<!--Wave Effects -->
<script src="/assets/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="/assets/js/custom.min.js"></script>
<!--Style Switcher -->
<script src="/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
<script>

    $(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
</script>
{!! Toastr::render() !!}
</body>

</html>
