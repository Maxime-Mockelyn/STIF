@extends('template')
@section("title")
    Listing des Gares
@stop
@section("header_styles")
    <link href="/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
@stop
@section("content")
<div class="panel">
    <div class="panel-heading">
        @yield("title")
        <div class="pull-right">
            <button class="btn btn-lg btn-primary" data-toggle="modal" data-target="#addGare"><i class="fa fa-plus"></i> Ajouter une gare</button>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table id="listeGare" class="table table-striped">
                <thead>
                    <tr>
                        <th class="text-center">Nom de la Gare</th>
                        <th class="text-center">Localisation</th>
                        <th class="text-center">Type de Gare</th>
                        <th class="text-center">Nombre d'accès possible</th>
                        <th class="text-center"><i class="fa fa-bars"></i> </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($gares as $gare)
                    <tr>
                        <td>{{ $gare->nameStation }}</td>
                        <td>
                            <strong>Latitude:</strong> {{ $gare->locateX }}<br>
                            <strong>Longitude:</strong> {{$gare->locateY}}
                        </td>
                        <td class="text-center">{{ \App\Http\Controllers\Gare\GareOtherController::getTypeGare($gare->typeStation) }}</td>
                        <td class="text-center">{{ \App\Http\Controllers\Gare\GareAccessOtherController::countAccessByStation($gare->id) }}</td>
                        <td class="text-center">
                            {{ Form::model($gare, ["route" => ["Gare.delete", $gare->id], "method" => "DELETE"]) }}
                            <a href="{{ route('Gare.show', $gare->id) }}" class="btn btn-lg btn-default"><i class="fa fa-eye"></i> </a>
                            <a href="{{ route('Gare.edit', $gare->id) }}" class="btn btn-lg btn-primary"><i class="fa fa-edit"></i> </a>
                            <button class="btn btn-lg btn-danger"><i class="fa fa-trash"></i> </button>
                            {{ Form::close() }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div id="addGare" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times-circle text-danger"></i> </button>
                <h4 class="modal-title" id="myLargeModalLabel"><i class="fa fa-plus-circle"></i> Nouveau gare</h4>
            </div>
            {{ Form::open(["route" => "Gare.store", "class" => "form-horizontal form-material"]) }}
            <div class="modal-body">
                <div class="form-group">
                    {{ Form::label('nameStation', "Nom de la gare", ["class" => "col-md-3"]) }}
                    <div class="col-md-6">
                        <select class="form-control select2" name="nameStation" >
                            @foreach(\App\Http\Controllers\Gare\GareOtherController::arrayListeGareBase() as $value)
                                <option value="{{ $value->id }}">{{ $value->libelle_gare }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('codeGare', "Code de la gare", ["class" => "col-md-3"]) }}
                    <div class="col-md-6">
                        {{ Form::text('codeGare', null, ["class" => "form-control"]) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('typeStation', "Type de Gare", ["class" => "col-md-3"]) }}
                    <div class="col-md-6">
                        <select class="form-control" name="typeStation">
                            <option value="0">Gare</option>
                            <option value="1">Arret</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-lg btn-success waves-effect text-left">Valider</button>
            </div>
            {{ Form::close() }}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@stop
@section("footer_scripts")
    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/custom-select/custom-select.min.js" type="text/javascript"></script>
    <script src="/assets/custom/js/gare/index.js"></script>
@stop    