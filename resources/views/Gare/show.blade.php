@extends('template')
@section("title")
    Gare: {{ $gare->nameStation }}
@stop
@section("header_styles")
    <link href="/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="//cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
@stop
@section("content")
<div class="row">
    <div class="col-md-4">
        <div class="white-box">
            <h3 class="box-title">
                <i class="fa fa-info-circle text-info"></i> Information de gare
            </h3>
            <table class="table">
                <tbody>
                    <tr>
                        <td style="font-weight: bold;text-align: right;">Nom de la Gare</td>
                        <td>{{ $gare->nameStation }}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;text-align: right;">Code de la Gare</td>
                        <td>{{ $gare->codeGare }}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;text-align: right;">Coordonnées GPS</td>
                        <td>X:{{ $gare->locateX }} |Y: {{ $gare->locateY }}</td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold;text-align: right;">Type Gare</td>
                        <td>{{ \App\Http\Controllers\Gare\GareOtherController::getTypeGare($gare->typeStation) }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-8">
        <div class="panel">
            <div class="panel-heading">
                Liste des gares d'accès
                <div class="pull-right">
                    <button class="btn btn-lg btn-primary" data-toggle="modal" data-target="#addAccess"><i class="fa fa-plus"></i> Ajouter un accès</button>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="listeAccess" class="table">
                        <thead>
                        <tr>
                            <th>Nom de la station</th>
                            <th><i class="fa fa-bars"></i> </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach(\App\Http\Controllers\Gare\GareAccessOtherController::arrayAccessByStation($gare->id) as $access)
                            <tr>
                                <td>{{ \App\Http\Controllers\Gare\GareOtherController::getInfoGare($access->stations_link, 'nameStation') }}</td>
                                <td>
                                    {{ Form::model($access, ["route" => ["Gare.Access.delete", $gare->id,$access->id], "method" => "DELETE"]) }}
                                    <button class="btn btn-lg btn-danger"><i class="fa fa-trash"></i> </button>
                                    {{ Form::close() }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="addAccess" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times-circle text-danger"></i> </button>
                <h4 class="modal-title" id="myLargeModalLabel"><i class="fa fa-plus-circle"></i> Nouveau gare</h4>
            </div>
            {{ Form::open(["route" => ["Gare.Access.store", $gare->id], "class" => "form-horizontal form-material"]) }}
            <div class="modal-body">
                <div class="form-group">
                    {{ Form::label('nameStation', "Nom de la Gare d'accès", ["class" => "col-md-3"]) }}
                    <div class="col-md-6">
                        <select class="form-control select2" name="nameStation" >
                            @foreach($gares as $value)
                                <option value="{{ $value->id }}">{{ $value->nameStation }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-lg btn-success waves-effect text-left">Valider</button>
            </div>
            {{ Form::close() }}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@stop
@section("footer_scripts")
    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/custom-select/custom-select.min.js" type="text/javascript"></script>
    <script src="/assets/custom/js/gare/show.js"></script>
@stop    