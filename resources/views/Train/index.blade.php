@extends('template')
@section("title")
    Liste du Parc Matériel
@stop
@section("header_styles")
    <link href="/assets/plugins/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
@stop
@section("content")
<div class="panel">
    <div class="panel-heading">
        @yield("title")
        <div class="pull-right">
            <button class="btn btn-primary" data-toggle="modal" data-target="#addTrain"><i class="fa fa-plus"></i> Ajouter un matériel</button>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table id="listeMateriel" class="table">
                <thead>
                    <tr>
                        <th>Type</th>
                        <th>Numéro du matériel</th>
                        <th><i class="fa fa-bars"></i> </th>
                    </tr>
                </thead>
                <tbody>
                @foreach($trains as $train)
                    <tr>
                        <td>{{ \App\Http\Controllers\Train\TrainTypeOtherController::getInfoTypeTrain($train->types_id, 'nameTypeTrain') }}</td>
                        <td>{{ $train->numTrain }}</td>
                        <td>
                            {{ Form::model($train, ["route" => ["Train.delete", $train->id], "method" => "DELETE"]) }}
                            <button class="btn btn-danger"><i class="fa fa-trash"></i> </button>
                            {{ Form::close() }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div id="addTrain" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times-circle text-danger"></i> </button>
                <h4 class="modal-title" id="myLargeModalLabel"><i class="fa fa-plus-circle"></i> Nouveau Train</h4>
            </div>
            {{ Form::open(["route" => "Train.store", "class" => "form-horizontal form-material"]) }}
            <div class="modal-body">
                <div class="form-group">
                    {{ Form::label('types_id', "Type de Train", ["class" => "col-md-3"]) }}
                    <div class="col-md-6">
                        {{ Form::select('types_id', \App\Http\Controllers\Train\TrainTypeOtherController::arrayTypesTrain("pluck"), null, ["class" => "form-control select2"]) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('numTrain', "Numéro du train", ["class" => "col-md-3"]) }}
                    <div class="col-md-6">
                        {{ Form::text('numTrain', null, ["class" => "form-control"]) }}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success waves-effect text-left">Valider</button>
            </div>
            {{ Form::close() }}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@stop
@section("footer_scripts")
    <script src="/assets/plugins/custom-select/custom-select.min.js" type="text/javascript"></script>
    <script src="/assets/custom/js/train/index.js"></script>
@stop    