@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Attention</strong> Certain champs n'ont pas été rempli correctement.
        <ul>
            @foreach ($errors->all() as $error)
                <li>{!! $error !!}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(session()->has('success'))
    <div role="alert" class="alert dark alert-success alert-icon alert-dismissible">
        <button aria-label="Close" data-dismiss="alert" class="close" type="button">
            <span aria-hidden="true">×</span>
        </button>
        <i class="icon md-check" aria-hidden="true"></i>
        <h4>{!! session('title') !!}</h4>
        <p>
            {!! session('success') !!}
        </p>
    </div>
@endif

@if(session()->has('error'))
    <div role="alert" class="alert dark alert-danger alert-icon alert-dismissible">
        <button aria-label="Close" data-dismiss="alert" class="close" type="button">
            <span aria-hidden="true">×</span>
        </button>
        <i class="icon md-remove" aria-hidden="true"></i>
        <h4>{!! session('title') !!}</h4>
        <p>
            {!! session('error') !!}
        </p>
    </div>
@endif

@if(session()->has('warning'))
    <div role="alert" class="alert dark alert-warning alert-icon alert-dismissible">
        <button aria-label="Close" data-dismiss="alert" class="close" type="button">
            <span aria-hidden="true">×</span>
        </button>
        <i class="icon md-alert-triangle" aria-hidden="true"></i>
        <h4>{!! session('title') !!}</h4>
        <p>
            {!! session('warning') !!}
        </p>
    </div>
@endif

@if(session()->has('info'))
    <div role="alert" class="alert dark alert-info alert-icon alert-dismissible">
        <button aria-label="Close" data-dismiss="alert" class="close" type="button">
            <span aria-hidden="true">×</span>
        </button>
        <i class="icon md-info" aria-hidden="true"></i>
        <h4>{!! session('title') !!}</h4>
        <p>
            {!! session('info') !!}
        </p>
    </div>
@endif
