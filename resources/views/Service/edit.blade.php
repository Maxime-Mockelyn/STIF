@extends('template')
@section("title")
    Edition d'un service
@stop
@section("header_styles")
    <link href="/assets/plugins/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
@stop
@section("content")
<div class="panel">
    <div class="panel-heading">
        @yield("title")
    </div>
    <div class="panel-body">
        {{ Form::model($service, ["route" => ["Service.update", $service->id], "class" => "form-horizontal form-material", "method" => "PUT"]) }}
            <div class="form-group">
                {{ Form::label("type_trajet", "Type de Trajet", ["class" => "col-md-3"]) }}
                <div class="col-md-6">
                    <div class="radio radio-primary">
                        {{ Form::radio('typeTrajet', 1, true, ["id" => "typeTrajet", "onclick" => "listeMateriel()"]) }}
                        <label for="typeTrajet">TER</label>
                    </div>
                    <div class="radio radio-primary">
                        {{ Form::radio('typeTrajet', 2, false, ["id" => "typeTrajet", "onclick" => "listeMateriel()"]) }}
                        <label for="typeTrajet">TGV</label>
                    </div>
                    <div class="radio radio-primary">
                        {{ Form::radio('typeTrajet', 3, false, ["id" => "typeTrajet", "onclick" => "listeMateriel()"]) }}
                        <label for="typeTrajet">SERVICE</label>
                    </div>
                    <div class="radio radio-primary">
                        {{ Form::radio('typeTrajet', 4, false, ["id" => "typeTrajet", "onclick" => "listeMateriel()"]) }}
                        <label for="typeTrajet">INTERCITE</label>
                    </div>
                    <div class="radio radio-primary">
                        {{ Form::radio('typeTrajet', 5, false, ["id" => "typeTrajet", "onclick" => "listeMateriel()"]) }}
                        <label for="typeTrajet">CORAIL</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{ Form::label("trains_id", "Matériel roulant", ["class" => "col-md-3"]) }}
                <div class="col-md-6">
                    <select id="trains_id" class="form-control select2" name="trains_id" required></select>
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('startStations_id', "Gare de départ", ["class" => "col-md-3"]) }}
                <div class="col-md-6">
                    {{ Form::select('startStations_id', $stationStarts, null, ["class" => "form-control select2", "id" => "startStations_id", "onchange" => "listeTerminus()", "required"]) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label("endStations_id", "Gare d'arrivé", ["class" => "col-md-3"]) }}
                <div class="col-md-6">
                    <!--<select id="endStations_id" class="form-control select2" name="endStations_id" required></select>-->
                    {{ Form::select('endStations_id', [], $service->endStations_id, ["class" => "form-control select2"]) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('timeStart', "Heure de Départ", ["class" => "col-md-3"]) }}
                <div class="col-md-3">
                    <div class="input-group clockpicker" data-placement="right" data-align="top" data-autoclose="true">
                        {{ Form::text('timeStart', null, ["class" => "form-control", "required"]) }}
                        <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                    </div>
                </div>
                {{ Form::label('timeEnd', "Heure d'arrivé", ["class" => "col-md-3"]) }}
                <div class="col-md-3">
                    <div class="input-group clockpicker" data-placement="left" data-align="top" data-autoclose="true">
                        {{ Form::text('timeEnd', null, ["class" => "form-control", "required"]) }}
                        <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                {{ Form::label("users_id", "Conducteur", ["class" => "col-md-3"]) }}
                <div class="col-md-6">
                    {{ Form::select('users_id', $users, null, ["class" => "form-control select2", "required"]) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('correspondanceLigne', "Correspondance avec Ligne TER", ["class" => "col-md-3"]) }}
                <div class="col-md-6">
                    {{ Form::text('correspondanceLigne', null, ["class" => "form-control"]) }}
                </div>
            </div>
            <button type="submit" class="btn btn-success waves-effect text-left">Valider</button>
        {{ Form::close() }}
    </div>
</div>
@stop
@section("footer_scripts")
    <script src="/assets/plugins/moment/moment.js"></script>
    <script src="/assets/plugins/custom-select/custom-select.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/clockpicker/dist/jquery-clockpicker.min.js"></script>
    <script src="/assets/custom/js/service/edit.js"></script>
@stop    