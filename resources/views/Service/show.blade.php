@extends('template')
@section("title")
    Service N°{{ $service->indice }}
@stop
@section("header_styles")
    <link rel="stylesheet" href="/assets/custom/plugins/ui-step/step.css">
    <link href="/assets/plugins/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
    <link href="/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/assets/plugins/html5-editor/bootstrap-wysihtml5.css" />
@stop
@section("content")
    <div class="row">
        <div class="col-md-3">
            <div class="white-box">
                <h3 class="title-box"><i class="fa fa-info-circle text-info"></i> Informations</h3>
                <hr>
                <table class="table no-border">
                    <tbody>
                        <tr>
                            <td style="font-weight: bold;text-align: right;">Indice</td>
                            <td>{{ $service->indice }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;text-align: right;">Correspondance avec la ligne TER</td>
                            <td>Ligne N° {{ $service->correspondanceLigne }}</td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;text-align: right;">Composition</td>
                            <td>
                                <strong>Type:</strong> {{ \App\Http\Controllers\Train\TrainTypeOtherController::getInfoTypeTrain(\App\Http\Controllers\Train\TrainOtherController::getInfoTrain($service->trains_id, 'types_id'), 'nameTypeTrain') }}<br>
                                <strong>Numéro:</strong> {{ \App\Http\Controllers\Train\TrainOtherController::getInfoTrain($service->trains_id, 'numTrain') }}
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;text-align: right;">Ligne</td>
                            <td>
                                <strong>De:</strong> {{ \App\Http\Controllers\Gare\GareOtherController::getInfoGare($service->startStations_id, 'codeGare') }}<br>
                                <strong>A:</strong> {{ \App\Http\Controllers\Gare\GareOtherController::getInfoGare($service->endStations_id, 'codeGare') }}<br>
                                <strong>Nombre d'arret:</strong> {{ \App\Http\Controllers\Service\ServiceOtherController::countLigneForService($service->id) }}
                            </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;text-align: right;">Horaire</td>
                            <td>
                                <strong>Heure de départ: </strong>{{ $service->timeStart }}<br>
                                <strong>Heure Théorique d'arrivé:</strong> {{ $service->timeEnd }}<br>
                                <strong>Durée du trajet:</strong> {{ \App\Http\Controllers\Service\ServiceOtherController::remainingTime($service->timeStart, $service->timeEnd) }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-9">
            <div class="white-box">
                <h2 class="title-box">
                    <i class="fa fa-file-text"></i> Feuille de Route
                    <span class="pull-right">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#addArret"><i class="fa fa-plus"></i> Ajouter un arret</button>
                    </span>
                </h2>
                <div class="table-responsive">
                    <table id="lignes" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>PK</th>
                                <th>Vitesse Maximum</th>
                                <th>Etablissement</th>
                                <th>Arrivé</th>
                                <th>Départ</th>
                                <th>Potentiel Min</th>
                                <th>Temps d'arret</th>
                                <th></th>
                            </tr>
                            <tr {!! \App\Http\Controllers\Service\ServiceLigneController::getTypeEtab($startLine->typeStation) !!}>
                                <td>{{ $startLine->pk }}</td>
                                <td class="text-center">
                                    @if(!empty($startLine->vitMax))
                                        {{ $startLine->vitMax }} Km/h
                                    @endif
                                </td>
                                <td>{{ \App\Http\Controllers\Gare\GareOtherController::getInfoGare($startLine->stations_id, 'nameStation') }}</td>
                                <td>
                                    @if(empty($startLine->arrivedTime))
                                        -
                                    @else
                                        {{ $startLine->arrivedTime }}
                                    @endif
                                </td>
                                <td>
                                    @if($startLine->departureTime == '00:00')
                                        -
                                    @else
                                        {{ $startLine->departureTime }}
                                    @endif
                                </td>
                                <td>
                                    @if($startLine->arrivedTime == "00:00")
                                        -
                                    @elseif($startLine->departureTime == "00:00")
                                        -
                                    @else
                                        {{ \App\Http\Controllers\Service\ServiceOtherController::DiffInReminingTime($startLine->id-1, $startLine->id) }}
                                    @endif
                                </td>
                                <td>{{ $startLine->tpsLast }}</td>
                                <td>
                                    <button id="annStartLine" class="btn palette-Purple-500 bg text-white" data-id="{{ $startLine->id }}"><i class="fa fa-play-circle"></i> </button>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($lignes as $ligne)
                            <tr {!! \App\Http\Controllers\Service\ServiceLigneController::getTypeEtab($ligne->typeStation) !!}>
                                <td>{{ $ligne->pk }}</td>
                                <td class="text-center">
                                    @if(!empty($ligne->vitMax))
                                        {{ $ligne->vitMax }} Km/h
                                    @endif
                                </td>
                                <td>
                                    {{ \App\Http\Controllers\Gare\GareOtherController::getInfoGare($ligne->stations_id, 'nameStation') }}
                                    @if(!empty($ligne->comment))
                                        <br>
                                        {!! $ligne->comment !!}
                                    @endif
                                </td>
                                <td>
                                    @if($ligne->arrivedTime == '00:00')
                                        -
                                    @else
                                        {{ $ligne->arrivedTime }}
                                    @endif
                                </td>
                                <td>
                                    @if($ligne->departureTime == '00:00')
                                        -
                                    @else
                                        {{ $ligne->departureTime }}
                                    @endif
                                </td>
                                <td>
                                    @if($ligne->arrivedTime == "00:00")
                                        -
                                    @elseif($ligne->departureTime == "00:00")
                                        -
                                    @else
                                        {{ \App\Http\Controllers\Service\ServiceOtherController::DiffInReminingTime($ligne->id-1, $ligne->id) }}
                                    @endif
                                </td>
                                <td>{{ $ligne->tpsLast }}</td>
                                <td>
                                    @if($ligne->typeStation == 1)
                                        <button id="startEvent" class="btn btn-xs btn-success" data-id="{{ $ligne->id }}" data-toggle="modal" data-target="getEventLigne"><i class="mdi mdi-play"></i> </button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr {!! \App\Http\Controllers\Service\ServiceLigneController::getTypeEtab($endLine->typeStation) !!}>
                            <td>{{ $endLine->pk }}</td>
                            <td class="text-center">
                                @if(!empty($endLine->vitMax))
                                    {{ $endLine->vitMax }} Km/h
                                @endif
                            </td>
                            <td>{{ \App\Http\Controllers\Gare\GareOtherController::getInfoGare($endLine->stations_id, 'nameStation') }}</td>
                            <td>
                                @if($endLine->arrivedTime == '00:00')
                                    -
                                @else
                                    {{ $endLine->arrivedTime }}
                                @endif
                            </td>
                            <td>
                                @if($endLine->departureTime == '00:00')
                                    -
                                @else
                                    {{ $endLine->departureTime }}
                                @endif
                            </td>
                            <td>
                                @if($endLine->arrivedTime == "00:00")
                                    -
                                @elseif($endLine->departureTime == "00:00")
                                    -
                                @else
                                    {{ \App\Http\Controllers\Service\ServiceOtherController::DiffInReminingTime($endLine->id-1, $endLine->id) }}
                                @endif
                            </td>
                            <td>{{ $endLine->tpsLast }}</td>
                            <td>
                                <button id="annEndLine" class="btn palette-Purple-500 bg text-white" data-id="{{ $endLine->id }}"><i class="fa fa-play-circle"></i> </button>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="getEventLigne" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" id="modal-event">

            </div>
        </div>
    </div>
    <div id="addArret" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times-circle text-danger"></i> </button>
                    <h4 class="modal-title" id="myLargeModalLabel"><i class="fa fa-plus-circle"></i> Nouvelle arret</h4>
                </div>
                {{ Form::model($service, ["route" => ["Service.Ligne.store", $service->id], "class" => "form-horizontal form-material"]) }}
                <div class="modal-body">
                    <div class="form-group">
                        {{ Form::label('stations_id', "Arret", ["class" => "col-md-3"]) }}
                        <div class="col-md-6">
                            <select class="form-control select2" name="stations_id" data-placeholder="Selectionnez un arret...">
                                <option value=""></option>
                                @foreach(\App\Http\Controllers\Gare\GareAccessOtherController::arrayAccessByStation($service->startStations_id) as $station)
                                    <option value="{{ $station->stations_link }}">{{ \App\Http\Controllers\Gare\GareOtherController::getInfoGare($station->stations_link, 'nameStation') }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label("comment", "Commentaire", ["class" => "col-md-3"]) }}
                        <div class="col-md-6">
                            {{ Form::textarea("comment", null, ["class" => "form-control comment"]) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('arrivedTime', "Heure de d'arrivé", ["class" => "col-md-3"]) }}
                        <div class="col-md-3">
                            <div class="input-group clockpicker" data-placement="right" data-align="top" data-autoclose="true">
                                {{ Form::text('arrivedTime', null, ["class" => "form-control", "required"]) }}
                                <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                            </div>
                        </div>
                        {{ Form::label('departureTime', "Heure de départ", ["class" => "col-md-3"]) }}
                        <div class="col-md-3">
                            <div class="input-group clockpicker" data-placement="right" data-align="top" data-autoclose="true">
                                {{ Form::text('departureTime', null, ["class" => "form-control", "required"]) }}
                                <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('typeStation', "Type de passage", ["class" => "col-md-3"]) }}
                        <div class="col-md-6">
                            <select class="form-control select2" name="typeStation" data-placeholder="Selectionnez un type de passage...">
                                <option value=""></option>
                                <option value="0">Passage</option>
                                <option value="1">Point de livraison</option>
                                <option value="2">Terminus</option>
                                <option value="3">Départ</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('pk', "Point Kilométrique", ["class" => "col-md-3"]) }}
                        <div class="col-md-2">
                            {{ Form::text('pk', null, ["class" => "form-control"]) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('vitMax', "Vitesse Maximum", ["class" => "col-md-3"]) }}
                        <div class="col-md-2">
                            {{ Form::text('vitMax', null, ["class" => "form-control"]) }}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success waves-effect text-left">Valider</button>
                </div>
                {{ Form::close() }}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div id="sound"></div>
@stop
@section("footer_scripts")
    <script src="/assets/plugins/moment/moment.js"></script>
    <script src="/assets/plugins/custom-select/custom-select.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/clockpicker/dist/jquery-clockpicker.min.js"></script>
    <script src="/assets/custom/plugins/ion/ion.sound.js"></script>
    <script src="/assets/custom/js/service/soundInitialize.js"></script>
    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/plugins/html5-editor/wysihtml5-0.3.0.js"></script>
    <script src="/assets/plugins/html5-editor/bootstrap-wysihtml5.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.comment').wysihtml5();
        });
    </script>
    <script src="/assets/custom/js/service/show.js"></script>
@stop    