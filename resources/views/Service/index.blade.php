@extends('template')
@section("title")
    Liste de tous le services
@stop
@section("header_styles")
    <link href="/assets/plugins/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
    <link href="/assets/plugins/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
    <link href="/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
@stop
@section("content")
<div class="panel">
    <div class="panel-heading">
        @yield("title")
        <div class="pull-right">
            <button class="btn btn-primary" data-toggle="modal" data-target="#addService"><i class="fa fa-plus"></i> Ajouter un nouveau service</button>
        </div>
    </div>
    <div class="panel-body">
        <div class="table-responsive">
            <table id="listeService" class="table table-striped">
                <thead>
                    <tr>
                        <th width="20%">Numéro de service</th>
                        <td width="10%">Correspondance</td>
                        <th width="10%">Train</th>
                        <th width="20%">Gare de Départ</th>
                        <th width="20%">Gare d'arrivée</th>
                        <th width="10%">Durée du trajet</th>
                        <th width="10%"><i class="fa fa-bars"></i> </th>
                    </tr>
                </thead>
                <tbody>
                @foreach($services as $service)
                    <tr>
                        <td>{{ $service->indice }}</td>
                        <td>{{ $service->correspondanceLigne }}</td>
                        <td>
                            <div class="row">
                                <div class="col-md-1">
                                    <img src="{{ \App\Http\Controllers\Train\TrainTypeOtherController::getImageType(\App\Http\Controllers\Train\TrainOtherController::getInfoTrain($service->trains_id, 'types_id')) }}" class="img-responsive" alt="">
                                </div>
                                <div class="col-md-11">
                                    <strong>Train: </strong>{{ \App\Http\Controllers\Train\TrainOtherController::getInfoTrain($service->trains_id, 'numTrain') }}<br>
                                    <strong>Type:</strong> {{ \App\Http\Controllers\Train\TrainTypeOtherController::getInfoTypeTrain(\App\Http\Controllers\Train\TrainOtherController::getInfoTrain($service->trains_id, 'types_id'), 'nameTypeTrain') }}
                                </div>
                            </div>
                        </td>
                        <td>{{ \App\Http\Controllers\Gare\GareOtherController::getInfoGare($service->startStations_id, 'nameStation') }}</td>
                        <td>{{ \App\Http\Controllers\Gare\GareOtherController::getInfoGare($service->endStations_id, 'nameStation') }}</td>
                        <td>{{ \App\Http\Controllers\Service\ServiceOtherController::remainingTime($service->timeStart, $service->timeEnd) }}</td>
                        <td>
                            {{ Form::model($service, ["route" => ["Service.delete", $service->id], "method" => "DELETE"]) }}
                            <a href="{{ route('Service.show', $service->id) }}" class="btn btn-default" data-toggle="tooltip" title="Acceder à ce service"><i class="fa fa-eye"></i> </a>
                            <a href="{{ route('Service.edit', $service->id) }}" class="btn btn-info" data-toggle="tooltip" title="Editer ce service"><i class="fa fa-edit"></i> </a>
                            <button class="btn btn-danger" data-toggle="tooltip" title="Supprimer ce service"><i class="fa fa-trash"></i> </button>
                            {{ Form::close() }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div id="addService" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times-circle text-danger"></i> </button>
                <h4 class="modal-title" id="myLargeModalLabel"><i class="fa fa-plus-circle"></i> Nouveau service</h4>
            </div>
            {{ Form::open(["route" => "Service.store", "class" => "form-horizontal form-material"]) }}
            <div class="modal-body">
                <div class="form-group">
                    {{ Form::label("type_trajet", "Type de Trajet", ["class" => "col-md-3"]) }}
                    <div class="col-md-6">
                        <div class="radio radio-primary">
                            {{ Form::radio('typeTrajet', 1, true, ["id" => "typeTrajet", "onclick" => "listeMateriel()"]) }}
                            <label for="typeTrajet">TER</label>
                        </div>
                        <div class="radio radio-primary">
                            {{ Form::radio('typeTrajet', 2, false, ["id" => "typeTrajet", "onclick" => "listeMateriel()"]) }}
                            <label for="typeTrajet">TGV</label>
                        </div>
                        <div class="radio radio-primary">
                            {{ Form::radio('typeTrajet', 3, false, ["id" => "typeTrajet", "onclick" => "listeMateriel()"]) }}
                            <label for="typeTrajet">SERVICE</label>
                        </div>
                        <div class="radio radio-primary">
                            {{ Form::radio('typeTrajet', 4, false, ["id" => "typeTrajet", "onclick" => "listeMateriel()"]) }}
                            <label for="typeTrajet">INTERCITE</label>
                        </div>
                        <div class="radio radio-primary">
                            {{ Form::radio('typeTrajet', 5, false, ["id" => "typeTrajet", "onclick" => "listeMateriel()"]) }}
                            <label for="typeTrajet">CORAIL</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label("trains_id", "Matériel roulant", ["class" => "col-md-3"]) }}
                    <div class="col-md-6">
                        <select id="trains_id" class="form-control select2" name="trains_id" required></select>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('startStations_id', "Gare de départ", ["class" => "col-md-3"]) }}
                    <div class="col-md-6">
                        {{ Form::select('startStations_id', $stationStarts, null, ["class" => "form-control select2", "id" => "startStations_id", "onchange" => "listeTerminus()", "required", "placeholder" => "Gare de Départ"]) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label("endStations_id", "Gare d'arrivé", ["class" => "col-md-3"]) }}
                    <div class="col-md-6">
                        <select id="endStations_id" class="form-control select2" name="endStations_id" required></select>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('timeStart', "Heure de Départ", ["class" => "col-md-3"]) }}
                    <div class="col-md-3">
                        <div class="input-group clockpicker" data-placement="right" data-align="top" data-autoclose="true">
                            {{ Form::text('timeStart', null, ["class" => "form-control", "required"]) }}
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                        </div>
                    </div>
                    {{ Form::label('timeEnd', "Heure d'arrivé", ["class" => "col-md-3"]) }}
                    <div class="col-md-3">
                        <div class="input-group clockpicker" data-placement="right" data-align="top" data-autoclose="true">
                            {{ Form::text('timeEnd', null, ["class" => "form-control", "required"]) }}
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label("users_id", "Conducteur", ["class" => "col-md-3"]) }}
                    <div class="col-md-6">
                        {{ Form::select('users_id', $users, null, ["class" => "form-control select2", "required"]) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('correspondanceLigne', "Correspondance avec Ligne TER", ["class" => "col-md-3"]) }}
                    <div class="col-md-6">
                        {{ Form::text('correspondanceLigne', null, ["class" => "form-control"]) }}
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success waves-effect text-left">Valider</button>
            </div>
            {{ Form::close() }}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@stop
@section("footer_scripts")
    <script src="/assets/plugins/moment/moment.js"></script>
    <script src="/assets/plugins/custom-select/custom-select.min.js" type="text/javascript"></script>
    <script src="/assets/plugins/clockpicker/dist/jquery-clockpicker.min.js"></script>
    <script src="/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/assets/custom/js/service/index.js"></script>
@stop    