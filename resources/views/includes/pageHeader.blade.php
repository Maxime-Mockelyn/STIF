<div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <h4 class="page-title">@yield("title")</h4> </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        @if($config->parent == 1)
            <button class="waves-effect waves-light btn-info btn-circle pull-right m-l-20" onclick="history.back()">
                <i class="fa fa-arrow-circle-left text-white"></i>
            </button>
        @endif
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">{{ env("APP_NAME") }}</a></li>
            @if(!empty($config->sector))
                <li><a href="javascript:void(0)">{{ $config->sectorView }}</a></li>
            @endif
            <li class="active">@yield('title')</li>
        </ol>
    </div>
    <!-- /.col-lg-12 -->
</div>