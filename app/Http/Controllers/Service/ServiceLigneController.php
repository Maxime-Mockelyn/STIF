<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Gare\GareOtherController;
use App\Http\Controllers\Train\TrainTypeOtherController;
use App\Model\Service\Service;
use App\Model\Service\ServiceLigne;
use App\Model\Service\ServiceSchedule;
use App\Model\Train;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Kamaln7\Toastr\Facades\Toastr;

class ServiceLigneController extends Controller
{
    // Route

    public function store(Request $request, $services_id){
        $this->validate($request, [
            "stations_id"   => "required",
            "arrivedTime"   => "required",
            "departureTime" => "required",
            "typeStation"   => "required",
            "pk"            => "required",
        ]);

        $tpsLast = self::calcTpsLast($request->arrivedTime, $request->departureTime);

        $ligne = ServiceLigne::create([
            "services_id"   => $services_id,
            "stations_id"   => $request->stations_id,
            "arrivedTime"   => $request->arrivedTime,
            "departureTime" => $request->departureTime,
            "tpsLast"       => $tpsLast,
            "typeStation"   => $request->typeStation,
            "pk"            => $request->pk,
            "vitMax"        => $request->vitMax,
            "comment"       => $request->comment
        ]);

        if($ligne){
            Toastr::success("Un arret à été ajouté à la feuille de route !");
            return redirect()->back();
        }
    }

    public function event($lignes_id){
        $ligne = ServiceLigne::find($lignes_id);
        $service = Service::find($ligne->services_id);
        $train = Train::find($service->trains_id);
        ob_start();
        ?>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times-circle text-danger"></i> </span></button>
            <h4 class="modal-title" id="myModalLabel">Arret: <?= GareOtherController::getInfoGare($ligne->stations_id, 'nameStation') ?></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td style="font-weight: bold;text-align: right;">Heure d'arrivé:</td>
                                <td><?= $ligne->arrivedTime ?></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;text-align: right;">Heure de départ:</td>
                                <td><?= $ligne->departureTime ?></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;text-align: right;">Point Kilométrique:</td>
                                <td><?= $ligne->pk ?></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;text-align: right;">Vitesse Maximum:</td>
                                <td><?= $ligne->vitMax ?> Km/h</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <button id="start" class="btn btn-block btn-lg btn-success" data-ligne-id="<?= $ligne->id ?>">Je vais à cette arret</button><br>
                    <button id="prochainArret" class="btn btn-block-btn-lg btn-primary" data-ligne-id="<?= $ligne->id ?>"><i class="fa fa-play-circle"></i> Prochaine Arret</button>
                    <button id="arret" class="btn btn-block-btn-lg btn-primary" data-ligne-id="<?= $ligne->id ?>"><i class="fa fa-play-circle"></i> Annonce Arret</button>
                </div>
            </div>
            <?php if(!empty($ligne->comment)): ?>
                <div class="white-box">
                    <h3 class="box-title">Commentaire</h3>
                    <?= $ligne->comment ?>
                </div>
            <?php endif; ?>
            <div id="progress">

            </div>
            <div id="audio"></div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
        </div>
        <script>
            $("#progress").hide();
            (function ($) {
                $("#start").on('click', function (e) {
                    var btn = $(this);
                    var ligne = btn.attr('data-ligne-id');
                    getprogress(ligne);
                });

                $("#prochainArret").on('click', function () {
                    var btn = $(this)
                    var ligne = btn.attr('data-ligne-id');
                    $.get('/service/ligne/'+ligne+'/sound/nextStop')
                        .done(function (data) {
                            $("#audio").html(data);
                        })
                })
                $("#arret").on('click', function () {
                    var btn = $(this)
                    var ligne = btn.attr('data-ligne-id');
                    $.get('/service/ligne/'+ligne+'/sound/stop')
                        .done(function (data) {
                            $("#audio").html(data);
                        })
                })
            })(jQuery);
            function getprogress(ligne){
                var progress = $("#progress");
                window.setInterval(function(){
                    $.get('/service/ligne/'+ligne+"/progress")
                        .done(function (data) {
                            $("#progress").show();
                            progress.html(data);
                        })
                }, 1000)
            }
        </script>
        <?php
        $content = ob_get_clean();
        return $content;
    }

    public function progress(Guard $auth, $lignes_id){

        $ligne = ServiceLigne::find($lignes_id);
        $service = Service::find($ligne->services_id);
        $remainingTime = ServiceOtherController::DiffInReminingTime($ligne->id-1, $ligne->id, true);

        if(self::countScheduleLigne($lignes_id) == 0){
            $schedule = ServiceSchedule::create([
                "users_id"          => $auth->id(),
                "services_id"       => $service->id,
                "service_lignes_id" => $lignes_id,
                "startTask"         => Carbon::now()->getTimestamp(),
                "endTask"           => 0,
                "etatTask"          => 1
            ]);
        }else{
            $schedule = ServiceSchedule::where('service_lignes_id', $lignes_id)->get()->last();
        }


        $demarrage = Carbon::createFromTimestamp($schedule->startTask);
        $start = $schedule->startTask;
        $diff = Carbon::now()->format('H:i:s');
        $now = strtotime($diff);

        $end = $demarrage->addSeconds($remainingTime)->getTimestamp();

        $calc = 100 / ($end - $start) * ($end - $now);

        ob_start();
        ?>
        <?php if($calc >= 66): ?>
            <div id="progressOuter" class="progress progress-lg">
                <div id="progressInner" class="progress-bar progress-bar-success" role="progressbar" style="width: <?= $calc ?>%;"> <span id="progressText"><?= round($calc, '0') ?>%</span> </div>
            </div>
        <?php elseif($calc < 65 && $calc >= 34): ?>
            <div id="progressOuter" class="progress progress-lg">
                <div id="progressInner" class="progress-bar progress-bar-warning" role="progressbar" style="width: <?= $calc ?>%;"> <span id="progressText"><?= round($calc, '0') ?>%</span> </div>
            </div>
        <?php elseif($calc < 34 && $calc >= 0): ?>
            <div id="progressOuter" class="progress progress-lg">
                <div id="progressInner" class="progress-bar progress-bar-danger" role="progressbar" style="width: <?= $calc ?>%;"> <span id="progressText"><?= round($calc, '0') ?>%</span> </div>
            </div>
        <?php else: ?>
            <span class="text-danger">Vous êtes en retard de <?= $diff; ?></span>
        <?php endif; ?>
        <?php
        $content = ob_get_clean();
        return $content;
    }

    // Static

    /**
     * Retourne une couleur en fonction du type d'établissement rencontrer
     * @param $value
     * @return string
     */
    public static function getTypeEtab($value){
        switch ($value){
            case 0: return "";
            case 1: return "class='palette-Blue-300 bg'";
            case 2: return "class='palette-Red-300 bg'";
            case 3: return "class='palette-Green-300 bg'";
        }
    }

    public static function getTypeEtabName($value){
        switch ($value){
            case 0: return "";
            case 1: return "passage";
            case 2: return "depart";
            case 3: return "terminus";
        }
    }

    /**
     * Reourne le nombre d'arret sur le services
     * @param $service_lignes_id
     * @return mixed
     */
    public static function countScheduleLigne($service_lignes_id){
        $count = ServiceSchedule::where('service_lignes_id', $service_lignes_id)->get()->count();
        return $count;
    }

    /**
     * Calcul le temps d'arret à une station en fonction de l'arrivée et du départ
     * @param $arrived
     * @param $departure
     * @return string
     */
    public static function calcTpsLast($arrived, $departure){
        $start = Carbon::createFromTimestamp(strtotime($arrived))->getTimestamp();
        $end = Carbon::createFromTimestamp(strtotime($departure))->getTimestamp();
        $diff = $end - $start;

        $calc = Carbon::createFromTimestamp($diff)->subHour()->format("H:i");
        return $calc;
    }
}
