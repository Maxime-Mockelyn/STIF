<?php

namespace App\Http\Controllers\Service;

use App\Model\Service\Service;
use App\Model\Service\ServiceLigne;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceOtherController extends Controller
{
    // Route



    // Static

    /**
     * Calcul le temps entre l'heure d'arrivé et de départ
     * @param $heureStart
     * @param $heureEnd
     * @return false|string
     */
    public static function remainingTime($heureStart, $heureEnd){
        $start = strtotime($heureStart);
        $end = strtotime($heureEnd);
        $diffin = strtotime(date('01:00'));

        return date("H:i", $end-$start-$diffin);
    }

    public static function DiffInReminingTime($ligneTimeStart, $ligneTimeEnd, $inSecond = false){

        $ligneStart = ServiceLigne::find($ligneTimeStart);
        if(empty($ligneStart->departureTime)){
            $departureTime = "00:00";
        }else{
            $departureTime = $ligneStart->departureTime;
        }
        $ligneEnd = ServiceLigne::find($ligneTimeEnd);
        if(empty($ligneEnd->arrivedTime)){
            $arrivedTime = "00:00";
        }else{
            $arrivedTime = $ligneEnd->arrivedTime;
        }

        $start = strtotime($departureTime);
        $end = strtotime($arrivedTime);

        if($inSecond == false){
            return Carbon::createFromTimestamp($end-$start)->subHour()->format('H:i');
        }else{
            return Carbon::createFromTimestamp($end-$start)->getTimestamp();
        }
    }

    /**
     * Génère un numéro de service en fonction du nom de la stations d'arrivée
     * @param $stationEndName
     * @return string
     */
    public static function genNumService($stationEndName){
        $replace = strtoupper(str_replace(' ', '', $stationEndName));
        $string = substr($replace, 0, 3);
        return $string.rand(0,9999999);

    }

    /**
     * Retourne le nombre d'arret au service associé
     * @param $services_id
     * @return mixed
     */
    public static function countLigneForService($services_id){
        $ligne = ServiceLigne::where('services_id', $services_id)->get()->count();

        return $ligne;
    }

    /**
     * Retourne le nombre de service comportant un numéro de train spécifique.
     * @param $trains_id
     * @return mixed
     */
    public static function countTrainForService($trains_id){
        $data = Service::where('trains_id', $trains_id)->get()->count();
        return $data;
    }
}
