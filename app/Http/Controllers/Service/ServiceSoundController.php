<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Gare\GareOtherController;
use App\Model\Service\Service;
use App\Model\Service\ServiceLigne;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceSoundController extends ServiceController
{
    // Route

    public function starter($lignes_id){
        $ligne = ServiceLigne::find($lignes_id);
        $service = Service::find($ligne->services_id);
        $lignes = ServiceLigne::where('services_id', $ligne->services_id)
            ->where('arrivedTime', '!=', '')
            ->where('departureTime', '!=', '')
            ->where('typeStation', 1)
            ->orderBy('departureTime', 'asc')
            ->get();
        $starterTime = 8000;
        $multi = 1500;
        $codeDestination = strtolower(GareOtherController::getInfoGare($service->endStations_id, 'codeGare'));
        $count = ServiceOtherController::countLigneForService($service->id);
        ob_start();
        ?>
        <script>
        window.setTimeout(function () {ion.sound.play("annonce_depart");}, 0);
        window.setTimeout(function () {ion.sound.play("<?= $codeDestination ?>");}, 8000);
        <?php if($count != 2): ?>
            window.setTimeout(function () {ion.sound.play("desserve");}, 9500);
            <?php foreach ($lignes as $k => $item): ?>
            window.setTimeout(function () {ion.sound.play('<?= strtolower(GareOtherController::getInfoGare($item->stations_id, 'codeGare')) ?>');}, 11000+(1500*<?= $k ?>));
            <?php endforeach; ?>
            window.setTimeout(function () {ion.sound.play('titre_transport');}, 11000+(1500*<?= $count-2 ?>));
        <?php else: ?>
        window.setTimeout(function () {ion.sound.play("sans_arret");}, 9000);
        window.setTimeout(function () {ion.sound.play('titre_transport');}, 13000);
        <?php endif; ?>

        </script>
        <?php
        $content = ob_get_clean();
        return $content;
    }

    public function nextStop($lignes_id){
        $ligne = ServiceLigne::find($lignes_id);
        ob_start();
        ?>
        <script>
            window.setTimeout(function () {ion.sound.play("prochaine_arret");}, 0);
            window.setTimeout(function () {ion.sound.play("<?= strtolower(GareOtherController::getInfoGare($ligne->stations_id, 'codeGare')) ?>");}, 1500);
        </script>
        <?php
        $content = ob_get_clean();
        return $content;
    }

    public function stop($lignes_id){
        $ligne = ServiceLigne::find($lignes_id);
        ob_start();
        ?>
        <script>
            window.setTimeout(function () {ion.sound.play("<?= strtolower(GareOtherController::getInfoGare($ligne->stations_id, 'codeGare')) ?>");}, 0);
        </script>
        <?php
        $content = ob_get_clean();
        return $content;
    }

    public function end($lignes_id){
        $ligne = ServiceLigne::find($lignes_id);
        ob_start();
        ?>
        <script>
            window.setTimeout(function () {ion.sound.play("annonce_terminus");}, 0);
            window.setTimeout(function () {ion.sound.play("<?= strtolower(GareOtherController::getInfoGare($ligne->stations_id, 'codeGare')) ?>");}, 4200);
        </script>
        <?php
        $content = ob_get_clean();
        return $content;
    }
}
