<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Gare\GareOtherController;
use App\Model\Gare\Gare;
use App\Model\Service\Service;
use App\Model\Service\ServiceLigne;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Kamaln7\Toastr\Facades\Toastr;

class ServiceController extends Controller
{
    public $sector;
    public $sectorView;

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->sector = 'service';
        $this->sectorView = 'Services';
    }

    public function index(){
        $config = (object) [
            "sector"    => $this->sector,
            "sectorView"=> $this->sectorView,
            "user"      => Auth::user(),
            "parent"    => 0
        ];

        $services = Service::all();
        $stationStarts = Gare::where('typeStation', 0)->get()->pluck('nameStation', 'id');
        $users = User::all()->pluck('name', 'id');

        return view("Service.index", compact('config', 'services', 'stationStarts', 'users'));
    }

    public function store(Request $request){
        $indice = ServiceOtherController::genNumService(GareOtherController::getInfoGare($request->endStations_id, 'nameStation'));

        $service = Service::create([
            "indice"                => $indice,
            "trains_id"             => $request->trains_id,
            "startStations_id"      => $request->startStations_id,
            "endStations_id"        => $request->endStations_id,
            "timeStart"             => $request->timeStart,
            "timeEnd"               => $request->timeEnd,
            "users_id"              => $request->users_id,
            "correspondanceLigne"   => $request->correspondanceLigne
        ]);

        $service = Service::all()->last();

        $addDepart = ServiceLigne::create([
            "services_id"   => $service->id,
            "stations_id"   => $request->startStations_id,
            "arrivedTime"   => null,
            "departureTime" => $request->timeStart,
            "tpsLast"       => "-",
            "typeStation"   => 3,
            "pk"            => 0
        ]);

        $addArrived = ServiceLigne::create([
            "services_id"   => $service->id,
            "stations_id"   => $request->endStations_id,
            "arrivedTime"   => $request->timeEnd,
            "departureTime" => null,
            "tpsLast"       => "-",
            "typeStation"   => 2,
            "pk"            => 0
        ]);

        if($service && $addDepart && $addArrived){
            Toastr::success("Le service <strong>".$indice."</strong> à été ajouté");
            return redirect()->back();
        }
    }

    public function show($services_id){
        $config = (object) [
            "sector"    => $this->sector,
            "sectorView"=> $this->sectorView,
            "user"      => Auth::user(),
            "parent"    => 1
        ];

        $service = Service::find($services_id)
            ->load('lignes');
        $lignes = ServiceLigne::where('services_id', $services_id)
            ->where('arrivedTime', '!=', '')
            ->where('departureTime', '!=', '')
            ->orderBy('departureTime', 'asc')
            ->get();
        $startLine = ServiceLigne::where('services_id', $services_id)
            ->where('departureTime', '!=', '')
            ->get()
            ->first();
        $endLine = ServiceLigne::where('services_id', $services_id)
            ->where('arrivedTime', '!=', '')
            ->get()
            ->first();

        //dd($startLine);

        return view('Service.show', compact('config', 'service', 'lignes', 'startLine', 'endLine'));

    }

    public function edit($services_id){
        $config = (object) [
            "sector"    => $this->sector,
            "sectorView"=> $this->sectorView,
            "user"      => Auth::user(),
            "parent"    => 1
        ];

        $service = Service::find($services_id);
        $stationStarts = Gare::where('typeStation', 1)->get()->pluck('nameStation', 'id');
        $users = User::all()->pluck('name', 'id');

        return view("Service.edit", compact("config", 'service', "stationStarts", "users"));
    }

    public function update(Request $request, $services_id){
        $service = Service::find($services_id);
        $update = $service->update([
            "trains_id"             => $request->trains_id,
            "startStations_id"      => $request->startStations_id,
            "endStations_id"        => $request->endStations_id,
            "timeStart"             => $request->timeStart,
            "timeEnd"               => $request->timeEnd,
            "users_id"              => $request->users_id,
            "correspondanceLigne"   => $request->correspondanceLigne
        ]);

        if($update){
            Toastr::success("Le service <strong>".$service->indice."</strong> à été édité");
            return redirect()->back();
        }
    }

    public function delete($services_id){
        $service = Service::find($services_id);
        $ligne = ServiceLigne::where('services_id', $services_id)->get();

        if(!empty($ligne)){
            $lignes = ServiceLigne::where('services_id', $services_id)->delete();
        }
        $delete = $service->delete();

        if($delete){
            Toastr::success("Le service <strong>".$service->indice."</strong> à été supprimé");
            return redirect()->back();
        }
    }
}
