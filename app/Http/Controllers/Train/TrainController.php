<?php

namespace App\Http\Controllers\Train;

use App\Http\Controllers\Service\ServiceOtherController;
use App\Model\Train;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Kamaln7\Toastr\Facades\Toastr;

class TrainController extends Controller
{
    public $sector;
    public $sectorView;

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->sector = 'train';
        $this->sectorView = 'Parc de Matériel';
    }

    public function index(){
        $config = (object) [
            "sector"    => $this->sector,
            "sectorView"=> $this->sectorView,
            "user"      => Auth::user(),
            "parent"    => 0
        ];

        $trains = Train::all();

        return view('Train.index', compact('config', 'trains'));
    }

    public function store(Request $request){
        $this->validate($request, [
            "types_id"  => "required",
            "numTrain"  => "required"
        ]);

        $train = Train::create([
            "types_id"      => $request->types_id,
            "numTrain"      => $request->numTrain
        ]);

        if($train){
            Toastr::success("Le Train N°<strong>".$request->numTrain."</strong> à été ajouté !");
            return redirect()->back();
        }
    }

    public function delete($trains_id){
        $train = Train::find($trains_id);
        if(ServiceOtherController::countTrainForService($trains_id) == 0){
            $del = $train->delete();
        }else{
            Toastr::warning("Ce train est actuellement utiliser dans un service.<br>Sa suppression est impossible !");
            return redirect()->back();
        }

        if($del){
            Toastr::success("Le train N°<strong>".$train->numTrain."</strong> à été supprimé !");
            return redirect()->back();
        }
    }
}
