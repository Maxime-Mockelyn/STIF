<?php

namespace App\Http\Controllers\Train;

use App\Model\TypeTrain;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TrainTypeOtherController extends TrainTypeController
{
    // Route



    // Static

    /**
     * Permet de recevoir en chaine de caractère le champs de la table type de train
     * @param $types_id
     * @param $field
     * @return mixed
     */
    public static function getInfoTypeTrain($types_id, $field){
        $type = TypeTrain::find($types_id);
        return $type->$field;
    }

    /**
     * Retourne l'adresse URI des images des types de train
     * @param $types_id
     * @return string
     */
    public static function getImageType($types_id){
        switch ($types_id){
            case 1: return "https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/Ter.svg/1200px-Ter.svg.png";
            case 2: return "https://upload.wikimedia.org/wikipedia/fr/0/01/Logo_TGV.jpg";
            case 3: return "https://upload.wikimedia.org/wikipedia/fr/thumb/9/92/Fret_SNCF.svg/1024px-Fret_SNCF.svg.png";
        }
    }

    /**
     * Permet de d'avoir la liste des types de train en static
     * @param string $mode
     * @~simple: Retourne uniquement le tableau
     * @~pluck: Retourne le tableau pour pluckage
     * @return bool|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public static function arrayTypesTrain($mode = "simple"){
        $db = TypeTrain::all();
        switch ($mode){
            case "simple":
                return $db;
                break;
            case "pluck":
                return $db->pluck('nameTypeTrain', 'id');
                break;
            default: return false;
        }
    }
}
