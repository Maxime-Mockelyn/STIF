<?php

namespace App\Http\Controllers\Train;

use App\Model\Train;
use App\Model\TypeTrain;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TrainOtherController extends TrainController
{
    // Route

    /**
     * Retourne un tableau des trains par types (Appel Ajax uniquement)
     * @param $types_id
     * @return string
     */
    public function listeByType($types_id){
        $trains = Train::where('types_id', $types_id)->get();
        ob_start();
        ?>
        <option value=""></option>
        <?php foreach ($trains as $train): ?>
            <option value="<?= $train->id; ?>">Train N°<?= $train->numTrain; ?></option>
        <?php endforeach; ?>
        <?php
        $content = ob_get_clean();
        return $content;
    }

    // Static

    /**
     * Retourne le champs visée de la table des trains en chaine de caractère
     * @param $trains_id
     * @param $field
     * @return mixed
     */
    public static function getInfoTrain($trains_id, $field){
        $train = Train::find($trains_id);
        return $train->$field;
    }
}
