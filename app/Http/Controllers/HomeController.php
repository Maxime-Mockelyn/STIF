<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public $sector;
    public $sectorView;

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->sector = '';
        $this->sectorView = 'Bienvenue';
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        $config = (object) [
            "sector"    => $this->sector,
            "sectorView"=> $this->sectorView,
            "user"      => Auth::user(),
            "parent"    => 0
        ];

        return view('home', compact('config'));
    }
}
