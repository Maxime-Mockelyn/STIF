<?php

namespace App\Http\Controllers\Gare;

use App\Model\Gare\GareAccess;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GareAccessOtherController extends GareAccessController
{
    // Route

    /**
     * Retourne les différentes options pour les terminus d'une stations données (Appel Ajax Uniquement)
     * @param $startStations_id
     * @return string
     */
    public function listeForTerminus($startStations_id){
        $gares = GareAccess::where('stations_id', $startStations_id)->get()->load('gare');
        ob_start();
        ?>
        <option value=""></option>
        <?php foreach ($gares as $gare): ?>
            <option value="<?= GareOtherController::getInfoGare($gare->stations_link, 'id') ?>"><?= GareOtherController::getInfoGare($gare->stations_link, 'nameStation') ?></option>
        <?php endforeach; ?>
        <?php
        $content = ob_get_clean();
        return $content;
    }

    // Static

    /**
     * Retourne le nombre d'accès possible pour une station données
     * @param $gare
     * @return mixed
     */
    public static function countAccessByStation($gare)
    {
        $count = GareAccess::where('stations_id', $gare)->get()->count();
        return $count;
    }

    /**
     * Retourne un tableau des accès d'une stations en mode static
     * @param $gares_id
     * @return mixed
     */
    public static function arrayAccessByStation($gares_id){
        $accesses = GareAccess::where('stations_id', $gares_id)->get()->load('gare');
        return $accesses;
    }

    public static function accessIsExist($stations_id, $gares_id){
        $access = GareAccess::where('stations_link', $stations_id)->where('stations_id', $gares_id)->get()->count();

        if($access != 0){
            return true;
        }else{
            return false;
        }
    }


}
