<?php

namespace App\Http\Controllers\Gare;

use App\Model\Gare\GareAccess;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kamaln7\Toastr\Facades\Toastr;

class GareAccessController extends Controller
{
    public function store(Request $request, $gares_id){
        $this->validate($request, [
            "nameStation"   => "required"
        ]);

        if(GareAccessOtherController::accessIsExist($request->nameStation, $gares_id) == true){
            Toastr::warning("Cette Gare existe déja dans l\'accès");
            return redirect()->back();
        }

        $store = GareAccess::create([
            "stations_id"   => $gares_id,
            "stations_link" => $request->nameStation
        ]);

        if($store){
            Toastr::success("Un accès à été ajouté !");
            return redirect()->back();
        }
    }

    public function delete($gares_id, $accesses_id){
        $access = GareAccess::find($accesses_id);
        $del = $access->delete();

        if($del){
            Toastr::success("L\'accès à la gare <strong>".GareOtherController::getInfoGare($access->stations_link, 'nameStation')."</strong> à été supprimé !");
            return redirect()->back();
        }
    }
}
