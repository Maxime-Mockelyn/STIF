<?php

namespace App\Http\Controllers\Gare;

use App\Model\Gare\BaseGare;
use App\Model\Gare\Gare;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GareOtherController extends Controller
{
    // Route



    // Static

    /**
     * Retourne le champs voulue de la table gare
     * @param $gares_id
     * @param $field
     * @return mixed
     */
    public static function getInfoGare($gares_id, $field){
        $gare = Gare::find($gares_id);
        return $gare->$field;
    }

    /**
     * Retourne le champs nameStation transformer en majuscule
     * @param $nameGare
     */
    public static function strampGare($nameGare){
        $replace = strtoupper(str_replace(' ', '', $nameGare));
    }

    /**
     * Retourne le type de gare
     * @param $typeStation
     * @return string
     */
    public static function getTypeGare($typeStation)
    {
        switch ($typeStation){
            case 0: return "Gare";
            case 1: return "Arret";
            default: return "NO ASSERT";
        }
    }

    /**
     * Retourne le tableau de la liste des gares de bases pour importation
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function arrayListeGareBase(){
        $bases = BaseGare::all();
        return $bases;
    }

    /**
     * Retourne le champs voulue de la table de la liste des gares de bases
     * @param $gares_id
     * @param $field
     * @return mixed
     */
    public static function getInfoGareBase($gares_id, $field){
        $base = BaseGare::find($gares_id);
        return $base->$field;
    }

    /**
     * Vérifie si la gare est déja dans la base de donnée
     * @param $nameStation
     * @return bool
     */
    public static function gareIsExist($nameStation){
        $gare = Gare::where('nameStation', $nameStation)->get()->count();

        if($gare != 0){
            return true;
        }else{
            return false;
        }
    }
}
