<?php

namespace App\Http\Controllers\Gare;

use App\Model\Gare\Gare;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Kamaln7\Toastr\Facades\Toastr;

class GareController extends Controller
{
    public $sector;
    public $sectorView;

    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->sector = 'gare';
        $this->sectorView = 'Gare';
    }

    public function index(){
        $config = (object) [
            "sector"    => $this->sector,
            "sectorView"=> $this->sectorView,
            "user"      => Auth::user(),
            "parent"    => 0
        ];

        $gares = Gare::all();

        return view('Gare.index', compact('config', 'gares'));
    }

    public function store(Request $request){
        $this->validate($request, [
            "nameStation"   => "required",
            "codeGare"      => "required",
            "typeStation"   => "required"
        ]);

        if(GareOtherController::gareIsExist($request->nameStation) == true){
            Toastr::warning("Cette Gare existe déja dans la base", "Attention");
            return redirect()->back();
        }

        $coordonneeBase = GareOtherController::getInfoGareBase($request->nameStation, 'coordonnees');
        $diffCoor = explode(',', $coordonneeBase);
        $latitude = $diffCoor[0];
        $longitude = $diffCoor[1];


        $gare = Gare::create([
            "nameStation"   => GareOtherController::getInfoGareBase($request->nameStation, 'libelle_gare'),
            "locateX"       => $latitude,
            "locateY"       => $longitude,
            "typeStation"   => $request->typeStation,
            "codeGare"      => $request->codeGare
        ]);

        if($gare){
            Toastr::success("Une gare à été ajouter !");
            return redirect()->back();
        }
    }

    public function show($gares_id){
        $config = (object) [
            "sector"    => $this->sector,
            "sectorView"=> $this->sectorView,
            "user"      => Auth::user(),
            "parent"    => 1
        ];

        $gare = Gare::find($gares_id);
        $gares = Gare::all();

        return view("Gare.show", compact('config', 'gare', 'gares'));
    }
}
