<?php

namespace App\Model\Service;

use App\Model\Gare\Gare;
use App\Model\Train;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $guarded = [];

    public function lignes(){
        return $this->belongsTo(ServiceLigne::class, 'services_id');
    }
}
