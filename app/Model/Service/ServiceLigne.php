<?php

namespace App\Model\Service;

use App\Model\Gare\Gare;
use Illuminate\Database\Eloquent\Model;

class ServiceLigne extends Model
{
    protected $guarded = [];

    public function service(){
        return $this->hasMany(Service::class, 'id');
    }
}
