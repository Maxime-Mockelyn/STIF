<?php

namespace App\Model\Gare;

use Illuminate\Database\Eloquent\Model;

class GareAccess extends Model
{
    protected $guarded = [];

    public function gare(){
        return $this->hasMany(Gare::class, 'id');
    }
}
