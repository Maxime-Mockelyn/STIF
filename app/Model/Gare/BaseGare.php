<?php

namespace App\Model\Gare;

use Illuminate\Database\Eloquent\Model;

class BaseGare extends Model
{
    protected $guarded = [];
}
