<?php

namespace App\Model\Gare;

use App\Model\Service\Service;
use App\Model\Service\ServiceLigne;
use Illuminate\Database\Eloquent\Model;

class Gare extends Model
{
    protected $guarded = [];

    public function accesses(){
        return $this->belongsTo(GareAccess::class, 'stations_id');
    }
}
