<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('users_id');
            $table->integer('services_id');
            $table->integer('service_lignes_id');
            $table->string('startTask');
            $table->string('endTask');
            $table->integer('etatTask')->default(0)->comment("0: Non Démarrer|1: En cours|2: Terminer");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_schedules');
    }
}
