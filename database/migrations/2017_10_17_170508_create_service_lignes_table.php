<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceLignesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_lignes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('services_id');
            $table->integer('stations_id');
            $table->string('arrivedTime')->nullable();
            $table->string('departureTime')->nullable();
            $table->string('tpsLast')->default(1);
            $table->integer('typeStation')->default(0)->comment("0: Passage |1: Point de livraison |2: Terminus |3: Depart");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_lignes');
    }
}
