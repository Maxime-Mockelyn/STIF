<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommentToServiceLigne extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_lignes', function (Blueprint $table) {
            $table->text('comment')->nullable()->comment("Ajout d'un commentaire si nécéssaire (Passage ou point particulier)");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_lignes', function (Blueprint $table) {
            $table->removeColumn('comment');
        });
    }
}
