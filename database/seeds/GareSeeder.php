<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GareSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gares')->insert(["namestation" => "Les Sables d'Olonne", "typeStation" => 1, "codeGare" => "LSO"]);
        DB::table('gares')->insert(["namestation" => "Olonne Sur Mer", "codeGare" => "OSM"]);
        DB::table('gares')->insert(["namestation" => "La Mothe Achard", "codeGare" => "LMAC"]);
        DB::table('gares')->insert(["namestation" => "La Roche sur Yon", "typeStation" => 1, "codeGare" => "LRSY"]);
        DB::table('gares')->insert(["namestation" => "Belleville - Vendée", "codeGare" => "BVEN"]);
        DB::table('gares')->insert(["namestation" => "L'herbergement - Les Brouzils", "codeGare" => "HLBZ"]);
        DB::table('gares')->insert(["namestation" => "Montaigu", "codeGare" => "MTU"]);
        DB::table('gares')->insert(["namestation" => "Clisson", "codeGare" => "CLSN"]);
        DB::table('gares')->insert(["namestation" => "Nantes", "typeStation" => 1, "codeGare" => "NAN"]);
        DB::table('gares')->insert(["namestation" => "Angers St-Laud", "codeGare" => "ASLD"]);
        DB::table('gares')->insert(["namestation" => "Le Mans", "codeGare" => "LMS"]);
        DB::table('gares')->insert(["namestation" => "Paris - Gare Montparnasse", "typeStation" => 1, "codeGare" => "PMP"]);
        DB::table('gares')->insert(["namestation" => "La Chaize-Le-Vicomte", "codeGare" => "LCLV"]);
        DB::table('gares')->insert(["namestation" => "Fougeré", "codeGare" => "FOUR"]);
        DB::table('gares')->insert(["namestation" => "Bournezeau", "codeGare" => "BOUZ"]);
        DB::table('gares')->insert(["namestation" => "Chantonnay", "typeStation" => 1, "codeGare" => "CHTY"]);
        DB::table('gares')->insert(["namestation" => "Pouzauges", "codeGare" => "PZGE"]);
        DB::table('gares')->insert(["namestation" => "Cerizay", "codeGare" => "CZAY"]);
        DB::table('gares')->insert(["namestation" => "Bressuire", "codeGare" => "BRSR"]);
        DB::table('gares')->insert(["namestation" => "Thouars", "codeGare" => "TARS"]);
        DB::table('gares')->insert(["namestation" => "Montreuil-Bellay", "codeGare" => "MBAY"]);
        DB::table('gares')->insert(["namestation" => "Saumur", "typeStation" => 1, "codeGare" => "SMU"]);
        DB::table('gares')->insert(["namestation" => "Port Boulet", "codeGare" => "PTBT"]);
        DB::table('gares')->insert(["namestation" => "Langeais", "codeGare" => "LGIS"]);
        DB::table('gares')->insert(["namestation" => "Cinq-Mars-la-Pile", "codeGare" => "CMLP"]);
        DB::table('gares')->insert(["namestation" => "Tours", "typeStation" => 1, "codeGare" => "TRS"]);
        DB::table('gares')->insert(["namestation" => "Luçon", "typeStation" => 1, "codeGare" => "LUN"]);
        DB::table('gares')->insert(["namestation" => "La Rochelle", "typeStation" => 1, "codeGare" => "LROL"]);
        DB::table('gares')->insert(["namestation" => "Rochefort", "typeStation" => 1, "codeGare" => "RCFT"]);
        DB::table('gares')->insert(["namestation" => "Saintes", "typeStation" => 1, "codeGare" => "STES"]);
        DB::table('gares')->insert(["namestation" => "Jonzac", "typeStation" => 1, "codeGare" => "JZC"]);
        DB::table('gares')->insert(["namestation" => "Bordeaux Saint-Jean", "typeStation" => 1, "codeGare" => "BSJ"]);
    }
}
