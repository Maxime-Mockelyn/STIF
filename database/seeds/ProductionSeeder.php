<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            "name"  => "MOCKELYN Maxime",
            "email" => "mmockelyn@stif.com",
            "password"=> bcrypt("1992_Maxime")
        ]);

        $this->call(TrainTypeSeeder::class);
    }
}
