<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServiceLigneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('service_lignes')->insert([
            "services_id"       => 1,
            "stations_id"       => 1,
            "arrivedTime"       => "00:00",
            "departureTime"     => "18:33",
            "tpsLast"           => "-",
            "typeStation"       => 3,
            "pk"                => 0
        ]);

        DB::table('service_lignes')->insert([
            "services_id"       => 1,
            "stations_id"       => 2,
            "arrivedTime"       => "18:38",
            "departureTime"     => "18:39",
            "typeStation"       => 1,
            "pk"                => 3.6
        ]);

        DB::table('service_lignes')->insert([
            "services_id"       => 1,
            "stations_id"       => 3,
            "arrivedTime"       => "18:46",
            "departureTime"     => "18:47",
            "typeStation"       => 1,
            "pk"                => 17.8
        ]);

        DB::table('service_lignes')->insert([
            "services_id"       => 1,
            "stations_id"       => 4,
            "arrivedTime"       => "18:59",
            "departureTime"     => "19:00",
            "typeStation"       => 1,
            "pk"                => 27.3
        ]);

        DB::table('service_lignes')->insert([
            "services_id"       => 1,
            "stations_id"       => 5,
            "arrivedTime"       => "19:08",
            "departureTime"     => "19:09",
            "typeStation"       => 1,
            "pk"                => 39.9
        ]);

        DB::table('service_lignes')->insert([
            "services_id"       => 1,
            "stations_id"       => 6,
            "arrivedTime"       => "19:16",
            "departureTime"     => "19:17",
            "typeStation"       => 1,
            "pk"                => 47.1
        ]);

        DB::table('service_lignes')->insert([
            "services_id"       => 1,
            "stations_id"       => 7,
            "arrivedTime"       => "19:23",
            "departureTime"     => "19:24",
            "typeStation"       => 1,
            "pk"                => 79.0
        ]);

        DB::table('service_lignes')->insert([
            "services_id"       => 1,
            "stations_id"       => 8,
            "arrivedTime"       => "19:31",
            "departureTime"     => "19:32",
            "typeStation"       => 1,
            "pk"                => 99.2
        ]);

        DB::table('service_lignes')->insert([
            "services_id"       => 1,
            "stations_id"       => 9,
            "arrivedTime"       => "19:50",
            "departureTime"     => "00:00",
            "tpsLast"           => "-",
            "typeStation"       => 2,
            "pk"                => 112.1
        ]);
    }
}
