<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GareAccessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gare_accesses')->insert(["stations_id" => 1, "stations_link" => 9]);
        DB::table('gare_accesses')->insert(["stations_id" => 1, "stations_link" => 12]);
        DB::table('gare_accesses')->insert(["stations_id" => 1, "stations_link" => 4]);

        DB::table('gare_accesses')->insert(["stations_id" => 4, "stations_link" => 1]);
        DB::table('gare_accesses')->insert(["stations_id" => 4, "stations_link" => 9]);
        DB::table('gare_accesses')->insert(["stations_id" => 4, "stations_link" => 12]);
        DB::table('gare_accesses')->insert(["stations_id" => 4, "stations_link" => 16]);
        DB::table('gare_accesses')->insert(["stations_id" => 4, "stations_link" => 28]);

        DB::table('gare_accesses')->insert(["stations_id" => 9, "stations_link" => 1]);
        DB::table('gare_accesses')->insert(["stations_id" => 9, "stations_link" => 4]);
    }
}
