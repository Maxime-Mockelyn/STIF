<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        DB::table('users')->insert([
            "name"  => "MOCKELYN Maxime",
            "email" => "mmockelyn@stif.com",
            "password"=> bcrypt("1992_Maxime")
        ]);

        $this->call(GareSeeder::class);
        $this->call(GareAccessSeeder::class);
        $this->call(TrainTypeSeeder::class);
        $this->call(TrainSeeder::class);
        $this->call(ServiceSeeder::class);
        $this->call(ServiceLigneSeeder::class);
    }
}
