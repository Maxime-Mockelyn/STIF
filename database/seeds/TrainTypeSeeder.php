<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TrainTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_trains')->insert(["nametypeTrain"  => "TER"]);
        DB::table('type_trains')->insert(["nametypeTrain"  => "TGV"]);
        DB::table('type_trains')->insert(["nametypeTrain"  => "SERVICE"]);
        DB::table('type_trains')->insert(["nametypeTrain"  => "INTERCITE"]);
        DB::table('type_trains')->insert(["nametypeTrain"  => "CORAIL"]);
    }
}
