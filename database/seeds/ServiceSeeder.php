<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert([
            "indice"                => "FGT785410",
            "users_id"              => 1,
            "trains_id"             => 1,
            "startStations_id"      => 1,
            "endStations_id"        => 9,
            "timeStart"             => "18:33",
            "timeEnd"               => "19:50"
        ]);
    }
}
