(function ($) {
    $(".select2").select2();
    $('.clockpicker').clockpicker();
    $("#listeService").dataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/French.json"
        }
    })
})(jQuery);

function listeMateriel(){
    var switchery = $("input[name='typeTrajet']:checked").val();
    var liste = $("#trains_id");

    $.get('/train/listeByType/'+switchery)
        .done(function (data) {
            liste.html(data);
        })
}

function listeTerminus(){
    var listingId = $("#startStations_id").val();
    var liste = $("#endStations_id");

    $.get('/gare/access/listeForTerminus/'+listingId)
        .done(function (data) {
            liste.html(data);
        })
}