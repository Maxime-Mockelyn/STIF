(function ($) {
    $(".select2").select2();
    $('.clockpicker').clockpicker();

    $("#lignes")
        .on('click', '#startEvent', function () {
            var btn = $(this);
            var getEventLigne = $("#getEventLigne");
            var ligneId = btn.attr('data-id');
            var content = $("#modal-event");
            getEventLigne.modal('show');
            $.get('/service/ligne/'+ligneId+'/event')
                .done(function (data) {
                    content.html(data)
                })
                .fail(function () {
                    content.html("<span class='text-center text-danger'><i class='fa fa-warning fa-lg'></i> Impossible de récupérer les éléments de l'arret</span>");
                })

        })
        .on('click', '#annStartLine', function () {
            var btn = $(this);
            var ligneId = btn.attr('data-id');
            //
            //window.setTimeout(function () {ion.sound.play("ancis");}, 8000);
            //window.setTimeout(function () {ion.sound.play("desserve");}, 9500);
            //window.setTimeout(function () {ion.sound.play('thare');}, 11000);
            //window.setTimeout(function () {ion.sound.play('mvlre');}, 12500);
            //window.setTimeout(function () {ion.sound.play('lceer');}, 14000);
            //window.setTimeout(function () {ion.sound.play('oudon');}, 15500);
            //window.setTimeout(function () {ion.sound.play('titre_transport');}, 17000);

            $.get('/service/ligne/'+ligneId+'/sound/starter')
                .done(function (data) {
                    window.setTimeout(function () {ion.sound.play("annonce_depart");}, 0);
                    $("#sound").html(data)
                })
        })
        .on('click', '#annEndLine', function () {
            var btn = $(this)
            var ligneId = btn.attr('data-id')

            $.get('/service/ligne/'+ligneId+'/sound/end')
                .done(function (data) {
                    window.setTimeout(function () {
                        ion.sound.play("annonce_terminus")
                    }, 0)
                    $("#sound").html(data)
                })
        })
})(jQuery);